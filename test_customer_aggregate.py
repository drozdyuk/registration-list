from unittest import TestCase
from customer import *
from event import *
import unittest

class CustomerTests(TestCase):
    def test_initial_event_non_vip(self):
        c = Customer(123, 'Joe Shmoe')
        self.assertEqual(type(c.initial), CustomerRegisteredEvent)
        self.assertEqual(c.initial.customer_id, 123)
        self.assertEqual(c.name, 'Joe Shmoe')

    def test_initial_event_vip(self):
        c = Customer(555, 'Joe Super', eligible_for_vip=True)
        evt = c.initial
        self.assertEqual(type(evt), VipCustomerRegisteredEvent)
        self.assertEqual(evt.customer_id, 555)
        self.assertEqual(evt.name, 'Joe Super')

    def test_change_name(self):
        c = Customer(123, 'Joe Shmoe')
        c.change_name('Marry Sc')
        self.assertEqual(len(c.changes), 1)
        evt = c.changes[0]
        self.assertEqual(type(evt), CustomerNameChangedEvent)
        self.assertEqual(evt.old_name, "Joe Shmoe")
        self.assertEqual(evt.new_name, 'Marry Sc')
        self.assertEqual(evt.customer_id, 123)
        
        # Make sure aggregate itself is actually updated
        self.assertEqual(c.name, 'Marry Sc')

    def test_changing_name_to_same_does_nothing(self):
        c = Customer(123, 'Abe Lincoln')
        c.change_name('Abe Lincoln')
        self.assertEqual(len(c.changes), 0)

    def test_changing_name_to_empty_does_nothing(self):
        c = Customer(123, 'Abe Lincold')
        c.change_name(None)
        self.assertEqual(len(c.changes), 0)
        c.change_name('')
        self.assertEqual(len(c.changes), 0)
        
    def test_preferred_on_non_vip(self):
        c = Customer(123, 'Ab Doe')
        self.assertRaises(CustomerNotEligibleForVipException, c.make_preferred)
        self.assertEqual(len(c.changes), 0)

    def test_preferred_on_vip(self):
        c = Customer(555, 'Joe Super', True)
        c.make_preferred()
        self.assertEqual(len(c.changes), 1)
        evt = c.changes[0]
        self.assertEqual(type(evt), CustomerBecamePreferredEvent)
        self.assertEqual(evt.customer_id, 555)

    def test_change_name_and_preferred(self):
        # construct history of the customer
        evt1 = CustomerBecamePreferredEvent(555)
        evt2 = CustomerNameChangedEvent(555, 'Ames', 'Eric Doe')
        
        # Construct customer and make sure he's not preferred
        c = Customer(555, 'Ames', True)
        self.assertFalse(c.preferred)
        
        c.replay([evt1, evt2])
        self.assertTrue(c.preferred)
        self.assertEqual(c.name, 'Eric Doe')
    

if __name__ == '__main__':
    unittest.main()        
