from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from event_store.infrastructure.persistence.pickle_serializer import PickleSerializer
from event_store.infrastructure.persistence.sql_event_store import SqlEventStore
from event_store.infrastructure.persistence.in_memory_event_store import InMemoryEventStore

def real_event_store():
    """Convinience function for hooking up real event store"""
    default_database_url = 'postgresql://postgres:test@localhost:5432/event_store'
    engine = create_engine(default_database_url)
        
    Session = sessionmaker(bind=engine, autocommit=False)
    session = Session()
    serializer = PickleSerializer()
    return SqlEventStore(session, serializer)

def in_memory_event_store():
    return InMemoryEventStore()
