from commands import *
from multipledispatch import dispatch
from customer import Customer

class RegistrationService(object):
    """Application service that allows customers to be registered,
    make them preferred, or delete them."""
    def __init__(self, customer_repository):
        self.customer_repository = customer_repository

    @dispatch(RegisterCustomerCommand)
    def handle(self, command):
        try:
            c = Customer(command.customer_id, command.customer_name)
            self.customer_repository.create(c)
        except ValueError:
            print("Customer already exists! Ignoring command.")
            raise

    @dispatch(RegisterVipCustomerCommand)
    def handle(self, command):
        try:
            c = Customer(command.customer_id, command.customer_name, eligible_for_vip=True)
            self.customer_repository.create(c)
        except ValueError:
            print("Customer already exists")
            raise
        
        
    @dispatch(ChangeCustomerNameCommand)
    def handle(self, command):
        try:
            customer = self.customer_repository.load(command.customer_id)
            customer.change_name(command.new_name)
            self.customer_repository.save(customer)
        except ValueError:
            print("Customer %s not found." % command.customer_id)

    @dispatch(UpgradeCustomerCommand)
    def handle(self, command):
        try:
            customer = self.customer_repository.load(command.customer_id)
            customer.make_preferred()
            self.customer_repository.save(customer)
        except ValueError:
            print("Customer %s not found." % command.customer_id)

    @dispatch(object)
    def handle(self, command):
        print("Unknown command. %s Ignoring." % repr(command))
