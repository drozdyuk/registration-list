from collections import namedtuple

RegisterVipCustomerCommand = namedtuple('RegisterVipCustomerCommand',
                                        ['customer_id',
                                         'customer_name'])

RegisterCustomerCommand = namedtuple('RegisterCustomerCommand', ['customer_id',
                                                                 'customer_name'])
ChangeCustomerNameCommand = namedtuple('ChangeCustomerNameCommand', ['customer_id',
                                                                     'new_name'])
UpgradeCustomerCommand = namedtuple('UpgradeCustomerCommand', ['customer_id'])
"""Upgrade customer to next rank of VIP"""
