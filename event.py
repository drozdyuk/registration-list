from collections import namedtuple

CustomerNameChangedEvent = namedtuple('CustomerNameChangedEvent',
                                      ['customer_id',
                                       'old_name',
                                       'new_name'])

VipCustomerRegisteredEvent = namedtuple('VipCustomerRegisteredEvent', ['customer_id', 'name'])
    

CustomerRegisteredEvent = namedtuple('CustomerRegisteredEvent',
                                     ['customer_id',
                                      'name'])

CustomerBecamePreferredEvent = namedtuple('CustomerBecamePreferredEvent',
                                     ['customer_id'])

CustomerUnregisteredEvent = namedtuple('CustomerUnregisteredEvent',
                                       ['customer_id','reason'])
