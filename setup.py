from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='registration-list',
    
    # Relevant: https://packaging.python.org/en/latest/single_source_version.html
    version='1.0.0a1',
    
    description='Registration List Sample Event Sourced Application',
    long_description=long_description,

    include_package_data=True,

    url='https://bitbucket.org/drozdyuk/registration-list',

    author='Andriy Drozdyuk',
    author_email='drozzy@gmail.com',

    license='Apache 2.0',

    # See: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',

        'Programming Language :: Python :: 2'
    ],

    keywords='eventstore eventsourcing',

    packages=find_packages(),

    install_requires=['event-store>=1.0.1a8',
                      'fake-factory',
                      'multipledispatch',
                      'pykka'],
                      
    # entry_points={
    #     'console_scripts': [
    #         'event_store = event_store.infrastructure.scripts.main:main'
    #     ]
    # }


)
