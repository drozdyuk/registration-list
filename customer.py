__author__ = 'Andriy Drozdyuk'
from event_store.infrastructure.reflection.reflection import *
from event import *
from multipledispatch import dispatch


class CustomerRepository(object):
    def __init__(self, event_store):
        self.event_store = event_store

    def create(self, customer):
        events = self.event_store.get_events_for(customer.customer_id)
        if not events:
            # New customer, as expected!
            self.event_store.save_changes(customer.customer_id,
                                          cls_name(Customer),
                                          customer.version,
                                          [customer.initial])
        else:
            # Customer already registered
            raise ValueError("Customer %s already exists. Cannot re-register." % customer.customer_id)
        
    def load(self, customer_id):
        events = self.event_store.get_events_for(customer_id)
        if not events:
            raise ValueError("Customer %s not found.")

        c = CustomerFactory().create(events[0])
        c.replay(events[1:])
        return c

    def save(self, customer):
        events = customer.changes
        self.event_store.save_changes(customer.customer_id,
                                      cls_name(Customer),
                                      customer.version,
                                      events)


class CustomerFactory(object):
    @dispatch(CustomerRegisteredEvent)
    def create(self, initial_event):
        """Create a customer entity from the initial event"""
        return Customer(initial_event.customer_id, initial_event.name)

    @dispatch(VipCustomerRegisteredEvent)
    def create(self, initial_event):
        return Customer(initial_event.customer_id, initial_event.name, eligible_for_vip=True)

    @dispatch(object)
    def create(self, initial_event):
        raise ValueError("Invalid initial event %s." % obj_name(initial))
    
class Customer(object):
    """Mock entity"""
    def __init__(self, customer_id, name, eligible_for_vip=False):
        self.customer_id = customer_id
        self.__name = name
        self.__eligible_for_vip = eligible_for_vip
        self.__preferred = False

        self.__changes = []

        self.__version = 0 # Version of this aggregate

        # Customer is registered untill the time he becomes no longer registered
        self.__registered = True

    @property
    def version(self):
        return self.__version

    def replay(self, events):
        for event in events:
            self.mutate(event)

    @property
    def initial(self):
        """Return the event that can be used to initialize this object."""
        if self.__eligible_for_vip:
            return VipCustomerRegisteredEvent(self.customer_id, self.__name)
        else:
            return CustomerRegisteredEvent(self.customer_id, self.__name)
        
        
    @property
    def changes(self):
        """Changes that this aggregate accumulated"""
        return self.__changes

    def apply(self, event):
        # Mutate our state
        self.mutate(event)
        
        # Add event to outstanding changes
        self.changes.append(event)

    def change_name(self, new_name):
        if (new_name is not None and new_name != '' and new_name != self.name):
            self.apply(CustomerNameChangedEvent(self.customer_id, self.name, new_name))


        self.__name = new_name if (new_name is not None and new_name != '') else self.__name

    def unregister(self, reason=''):
        self.apply(CustomerUnregisteredEvent(self.customer_id, reason))


    def mutate(self, event):
        if not self.__registered:
            raise NonRegisteredCustomerException()
        
        if isinstance(event, CustomerNameChangedEvent):
            self.__name = event.new_name
        elif isinstance(event, CustomerBecamePreferredEvent):
            self.__preferred = True
        elif isinstance(event, CustomerUnregisteredEvent):
            self.__registered = False
        else:
            raise ValueError("Uknown event applied to customer: %s" % obj_name(event))
        self.__version += 1

    @property
    def name(self):
        return self.__name
    
    @property
    def preferred(self):
        return self.__preferred

    def make_preferred(self):
        if self.__eligible_for_vip:
            self.apply(CustomerBecamePreferredEvent(self.customer_id))
        else:
            raise CustomerNotEligibleForVipException()

        
class NonRegisteredCustomerException(Exception):
    """Customer is no longer registered, and some operation was attempted on it."""
    pass

class CustomerNotEligibleForVipException(Exception):
    """This customer is not eligible for vip status"""
    pass
